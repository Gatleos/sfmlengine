#include "States.hpp"
#include "Collision.hpp"
#include "coord.hpp"

static Polygon p;
static sf::RenderStates states;
static sf::Vector2f mousePos;

void drawPolygon(sf::RenderTarget& target, Polygon& poly, sf::RenderStates states = sf::RenderStates::Default, sf::Color color = sf::Color::White) {
	static sf::VertexArray vert;
	vert.setPrimitiveType(sf::PrimitiveType::LinesStrip);
	vert.clear();
	for (int a = 0; a <= poly.vertNum(); a++) {
		vert.append(poly.getPoint(a));
		vert[a].color = color;
	}
	target.draw(vert, states);
}

void TestState::init() {
	p.addPoint(sf::Vector2f(10.0f, -20.0f));
	p.addPoint(sf::Vector2f(30.0f, 0.0f));
	p.addPoint(sf::Vector2f(10.0f, 20.0f));
	p.addPoint(sf::Vector2f(-30.0f, 0.0f));
	if (!p.isConvex()) {
		int x = 0;
	}
	if (p.contains(sf::Vector2f(11.0f, 0.0f))) {
		int x = 0;
	}
	p.aabb.center = sf::Vector2f(100.0f, 100.0f);
}
void TestState::end() {
}
void TestState::update() {
}
void TestState::render(sf::RenderWindow &window) {
	if (p.contains(mousePos)) {
		drawPolygon(window, p, states, sf::Color::Green);
	}
	else {
		drawPolygon(window, p, states);
	}
}
void TestState::input(sf::Event &e) {
	if (e.type == sf::Event::MouseMoved) {
		mousePos.x = e.mouseMove.x;
		mousePos.y = e.mouseMove.y;
	}
}
