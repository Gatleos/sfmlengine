#include <iostream>
#include "SFMLEngine.hpp"



SFMLEngine& SFMLEngine::instance() {
	static SFMLEngine instance;
	return instance;
}
void SFMLEngine::init(sf::RenderWindow* windowset) {
	window = windowset;
	window->setFramerateLimit(60);
	clearColor = sf::Color::Black;
}
void SFMLEngine::start() {
	try {
		sf::Clock fr;
		while (window->isOpen()) {
			while (window->pollEvent(event)) {
				if (event.type == sf::Event::Closed) {
					window->close();
					//return;
				}
				else states.top()->input(event);
			}
			//
			lastFrame = fr.restart();
			states.top()->update();
			//
			window->clear(clearColor);
			states.top()->render(*window);
			window->display();
		}
	}
	catch(std::runtime_error e) {
		std::cerr << e.what();
		window->close();
	}
}

void SFMLEngine::pushState(GameState* newState) {
	if (states.size())
		newState->prev = states.top();
	states.push(newState);
	states.top()->engine = this;
	states.top()->init();
}
void SFMLEngine::popState() {
	if (!states.empty()) {
		states.top()->end();
		delete states.top();
		states.pop();
		while (window->pollEvent(event));
	}
}
void SFMLEngine::popAllStates() {
	if (states.empty()) return;
	while (states.size()) {
		states.top()->end();
		delete states.top();
		states.pop();
	}
}
int SFMLEngine::getFPS() const {
	return (int)(1000000LL / std::max(lastFrame.asMicroseconds(), 1LL));
}
const sf::Time& SFMLEngine::getLastTick() const {
	return lastFrame;
}
void SFMLEngine::setClearColor(sf::Color& set) {
	clearColor = set;
}
