#include "Polygon.hpp"
#include "coord.hpp"

Polygon::Polygon() {
	points.push_back(sf::Vector2f());
}

sf::Vector2f Polygon::getPoint(int index) const {
	return points[index] + aabb.center;
}

sf::Vector2f Polygon::getRelPoint(int index) const {
	return points[index];
}

int Polygon::vertNum() const {
	return points.size() - 1;
}

void Polygon::addPoint(sf::Vector2f point) {
	if (points.size() < 2) {
		points[0] = point;
	}
	points.insert(points.begin() + points.size() - 1 , point);
	sf::Vector2f relPos = point - aabb.center;
	relPos.x = std::abs(relPos.x);
	if (relPos.x > aabb.extents.x) {
		aabb.extents.x = relPos.x;
	}
	relPos.y = std::abs(relPos.y);
	if (relPos.y > aabb.extents.y) {
		aabb.extents.y = relPos.y;
	}
}

void Polygon::clearPoints() {
	points.clear();
	points.push_back(sf::Vector2f());
	aabb.extents = sf::Vector2f(0.0f, 0.0f);
}

bool Polygon::isConvex() const {
	char dir = 0;
	float angle = 0.0f, lastAngle = 0.0f;
	float lastDist = 0.0f;
	for (int a = 0; a < points.size() - 1; a++) {
		sf::Vector2f edge = points[a + 1] - points[a];
		angle = coord::theta(edge);
		float dist = coord::angleDistance(angle, lastAngle);
		if (lastDist != 0.0f && std::signbit(lastDist) != std::signbit(dist)) {
			return false;
		}
		if (a != 0) {
			lastDist = dist;
		}
		lastAngle = angle;
	}
	return true;
}

bool Polygon::contains(sf::Vector2f point) const {
	bool c = false;
	for (int a = 0; a < points.size() - 1; a++) {
		sf::Vector2f point1 = getPoint(a);
		sf::Vector2f point2 = getPoint(a+1);
		if (((point2.y > point.y) != (point1.y > point.y)) &&
			(point.x < (point1.x - point2.x) * (point.y - point2.y) / (point1.y - point2.y) + point2.x)) {
			c = !c;
		}
	}
	return c;
}
