#pragma once
#include <vector>
#include <SFML/Graphics.hpp>
#include "AABB.hpp"

class Polygon {
	std::vector<sf::Vector2f> points;
public:
	AABB aabb;
	Polygon();
	sf::Vector2f getPoint(int index) const;
	sf::Vector2f getRelPoint(int index) const;
	int vertNum() const;
	void addPoint(sf::Vector2f point);
	void clearPoints();
	bool isConvex() const;
	bool contains(sf::Vector2f point) const;
};