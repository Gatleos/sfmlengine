#pragma once
#include "AABB.hpp"
#include "Polygon.hpp"

namespace coll {

	static AABB minkowskiDiff(const AABB& a, const AABB& b) {
		sf::Vector2f topLeft = a.min() - b.max();
		sf::Vector2f extentSum = a.extents + b.extents;
		return AABB(topLeft + extentSum, extentSum);
	}



}