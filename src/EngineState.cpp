#include "States.hpp"
#include "Collision.hpp"
#include "coord.hpp"

//static sf::Vector2f startPosition(-0.000707847183, 90.0007019);
static sf::Vector2f startPosition(200.0f, 154.0f);
static AABB a(startPosition, sf::Vector2f(20.0f, 20.0f));
static AABB b(sf::Vector2f(150.0f, 120.0f), sf::Vector2f(80.0f, 10.0f));
static AABB c;
static sf::Vector2f windowSize;
static sf::RenderStates states;
static sf::VertexArray va;
static const float speed = 5.0f;
static sf::Vector2f velocity;
static sf::VertexArray vert;
static const sf::Vector2f gravity = { 0.0f, 0.0f };
static const sf::Vector2f zeroVector;
static sf::Font font;
static sf::Text text;

void drawAABB(sf::RenderTarget& target, AABB& aabb, sf::Color color = sf::Color::White, sf::RenderStates states = sf::RenderStates::Default) {
	static sf::RectangleShape rect;
	rect.setOrigin(aabb.extents);
	rect.setPosition(aabb.center);
	rect.setSize(aabb.size());
	rect.setFillColor(sf::Color::Transparent);
	rect.setOutlineColor(color);
	rect.setOutlineThickness(-1.0f);
	target.draw(rect, states);
}

void drawVector(sf::RenderTarget& target, sf::Vector2f origin, sf::Vector2f end, sf::Color color = sf::Color::Red, sf::RenderStates states = sf::RenderStates::Default) {
	vert[0].position = origin;
	vert[1].position = end;
	vert[2].position = end;
	vert[2].color = color;
	target.draw(vert, states);
}

void EngineState::init() {
	font.loadFromFile("data/DejaVuSans.ttf");
	text.setFont(font);
	vert.resize(3);
	vert.setPrimitiveType(sf::PrimitiveType::LinesStrip);
	engine->window->setKeyRepeatEnabled(false);
	windowSize = (sf::Vector2f)engine->window->getSize();
	states.transform.translate(windowSize / 2.0f);
	va.resize(1);
}
void EngineState::end() {
}
void EngineState::update() {
	c = coll::minkowskiDiff(a, b);
	if (a.intersects(b)) {
		int x = 0;
	}
	sf::Vector2f move = velocity + gravity;
	Collision intersection = c.getRayIntersectionFraction(zeroVector, move*-1.0f);
	if (intersection.time < std::numeric_limits<float>::infinity()) {
		sf::Vector2f test = coord::project(move, sf::Vector2f(100.0f, -100.0f));
		sf::Vector2f parallel = coord::project(move, coord::rightNormal(intersection.normal));
		sf::Vector2f perpendicular = coord::project(move, intersection.normal);
		float m = coord::mag(move);
		sf::Vector2f v = parallel + perpendicular * (intersection.time - (0.001f / m));
		sf::Vector2f oldCenter = a.center;
		a.center += v;
		if (a.intersects(b)) {
			float aa = a.center.y + a.extents.y;
			float bb = b.center.y - b.extents.y;
			bb -= aa;
			a.intersects(b);
		}
	}
	else {
		a.center += move;
	}
	c = coll::minkowskiDiff(a, b);
	text.setString(std::to_string(a.center.x) + ", " + std::to_string(a.center.y));
}
void EngineState::render(sf::RenderWindow &window) {
	sf::Vector2f move = (velocity + gravity) * -1.0f;
	if (c.contains(sf::Vector2f(0.0f, 0.0f))) {
		drawAABB(window, a, sf::Color::Green);
	}
	else {
		drawAABB(window, a);
	}
	drawAABB(window, b, sf::Color(0, 255, 255));
	drawAABB(window, c, sf::Color(180, 40, 40), states);
	window.draw(va, states);
	drawVector(window, sf::Vector2f(0.0f, 0.0f), move, sf::Color::Red, states);
	window.draw(text);
}
void EngineState::input(sf::Event &e) {
	if (e.type == sf::Event::KeyPressed) {
		if (e.key.code == sf::Keyboard::Left) {
			velocity.x -= speed;
		}
		else if (e.key.code == sf::Keyboard::Right) {
			velocity.x += speed;
		}
		else if (e.key.code == sf::Keyboard::Up) {
			velocity.y -= speed;
		}
		else if (e.key.code == sf::Keyboard::Down) {
			velocity.y += speed;
		}
		else if (e.key.code == sf::Keyboard::Return) {
			a.center = startPosition;
		}
	}
	else if (e.type == sf::Event::KeyReleased) {
		if (e.key.code == sf::Keyboard::Left) {
			velocity.x += speed;
		}
		else if (e.key.code == sf::Keyboard::Right) {
			velocity.x -= speed;
		}
		else if (e.key.code == sf::Keyboard::Up) {
			velocity.y += speed;
		}
		else if (e.key.code == sf::Keyboard::Down) {
			velocity.y -= speed;
		}
	}
	else if (e.type == sf::Event::MouseMoved) {
		
	}
}
