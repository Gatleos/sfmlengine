#include <cstring>
#include <iostream>
#include "VERSION.hpp"
#include "States.hpp"

int main(int argc, char* argv[])
{
	if (argc == 2) {
		if (!std::strcmp(argv[1], "-v")) {
			std::cout << PROG_VERSION << " compiled " << __DATE__ << " " << __TIME__;
		}
	}
	else {
		// Create our game engine
		auto& s = SFMLEngine::instance();
		sf::RenderWindow window(sf::VideoMode(640, 512), "", sf::Style::Default, sf::ContextSettings());
		s.init(&window);
		s.pushState(new TestState());
		s.start();
	}
	return 0;
}
