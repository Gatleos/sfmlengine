#pragma once

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

struct Collision {
	sf::Vector2f normal;
	float time;
	Collision(sf::Vector2f normal, float time) : normal(normal), time(time) {}
};

class AABB {
public:
	sf::Vector2f center;
	sf::Vector2f extents;
	//
	AABB();
	AABB(sf::Vector2f center, sf::Vector2f extents);
	sf::Vector2f min() const;
	sf::Vector2f max() const;
	sf::Vector2f size() const;
	bool contains(sf::Vector2f point) const;
	bool intersects(const AABB& aabb) const;
	static float getRayIntersectionFractionOfFirstRay(sf::Vector2f originA, sf::Vector2f endA, sf::Vector2f originB, sf::Vector2f endB);
	Collision getRayIntersectionFraction(sf::Vector2f origin, sf::Vector2f direction);
};
