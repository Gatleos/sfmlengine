#ifndef STATES_H
#define STATES_H

#include "SFMLEngine.hpp"


class EngineState: public GameState {
public:
	void init();
	void end();
	void update();
	void render(sf::RenderWindow &window);
	void input(sf::Event &e);
}; // EngineState

class TestState : public GameState {
public:
	void init();
	void end();
	void update();
	void render(sf::RenderWindow &window);
	void input(sf::Event &e);
}; // EngineState

#endif