#pragma once

#include <SFML/System.hpp>

namespace coord {
	const float PI = 3.1415926535f;
	const float TAU = 6.2831853071f;
	const float radToDeg = 57.2958f;

	static sf::Vector2f cartToPolar(const sf::Vector2f& cart) {
		return sf::Vector2f(std::sqrt(std::pow(cart.x, 2.0f) + std::pow(cart.y, 2.0f)),
			std::atan2(cart.y, cart.x));
	}

	static sf::Vector2f polarToCart(const sf::Vector2f& polar) {
		return sf::Vector2f(polar.x * std::cos(polar.y), polar.x * std::sin(polar.y));
	}

	static float theta(const sf::Vector2f& cart) {
		return std::atan2(cart.y, cart.x);
	}

	static float mag(const sf::Vector2f& cart) {
		return std::sqrt(std::pow(cart.x, 2.0f) + std::pow(cart.y, 2.0f));
	}

	static sf::Vector2f setTheta(const sf::Vector2f& cart, float theta) {
		sf::Vector2f polar = cartToPolar(cart);
		polar.y = theta;
		return polarToCart(polar);
	}

	static sf::Vector2f setMag(const sf::Vector2f& cart, float mag) {
		sf::Vector2f polar = cartToPolar(cart);
		polar.x = mag;
		return polarToCart(polar);
	}

	static sf::Vector2f normalize(const sf::Vector2f& cart) {
		return setMag(cart, 1.0f);
	}

	static float angleDistance(float radA, float radB) {
		float dist = radA - radB;
		dist += (dist > PI) ? -TAU : (dist < -PI) ? TAU : 0.0f;
		return dist;
	}

	static float dot(sf::Vector2f cartA, sf::Vector2f cartB) {
		return cartA.x * cartB.x + cartA.y * cartB.y;
	}

	static float cross(sf::Vector2f cartA, sf::Vector2f cartB) {
		return cartA.x * cartB.y - cartA.y * cartB.x;
	}

	static sf::Vector2f project(sf::Vector2f cartA, sf::Vector2f cartB) {
		sf::Vector2f proj;
		float dp = dot(cartA, cartB);
		proj.x = (dp / (cartB.x*cartB.x + cartB.y*cartB.y)) * cartB.x;
		proj.y = (dp / (cartB.x*cartB.x + cartB.y*cartB.y)) * cartB.y;
		return proj;
	}

	static sf::Vector2f leftNormal(const sf::Vector2f& cart) {
		sf::Vector2f ln;
		ln.x = cart.y;
		ln.y = -cart.x;
		return ln;
	}

	static sf::Vector2f rightNormal(const sf::Vector2f& cart) {
		sf::Vector2f rn;
		rn.x = -cart.y;
		rn.y = cart.x;
		return rn;
	}
}