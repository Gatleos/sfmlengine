#include <limits>
#include "AABB.hpp"
#include "coord.hpp"

float dot(sf::Vector2f a, sf::Vector2f b) {
	return a.x * b.x + a.y * b.y;
}

AABB::AABB() {
}

AABB::AABB(sf::Vector2f center, sf::Vector2f extents) :
center(center), extents(extents) {}

sf::Vector2f AABB::min() const {
	return sf::Vector2f(center.x - extents.x, center.y - extents.y);
}

sf::Vector2f AABB::max() const {
	return sf::Vector2f(center.x + extents.x, center.y + extents.y);
}

sf::Vector2f AABB::size() const {
	return extents * 2.0f;
}

bool AABB::contains(sf::Vector2f point) const {
	if (std::abs(point.x - center.x) > extents.x) {
		return false;
	}
	if (std::abs(point.y - center.y) > extents.y) {
		return false;
	}
	return true;
}

bool AABB::intersects(const AABB & aabb) const {
	sf::Vector2f relPos(std::abs(center.x - aabb.center.x), std::abs(center.y - aabb.center.y));
	if (relPos.x > (extents.x + aabb.extents.x)) {
		return false;
	}
	if (relPos.y > (extents.y + aabb.extents.y)) {
		return false;
	}
	return true;
}

float AABB::getRayIntersectionFractionOfFirstRay(sf::Vector2f originA, sf::Vector2f endA, sf::Vector2f originB, sf::Vector2f endB) {
	sf::Vector2f r = endA - originA;
	sf::Vector2f s = endB - originB;

	float numerator = coord::cross((originB - originA), r);
	float denominator = coord::cross(r, s);

	if (numerator == 0 && denominator == 0) {
		// the lines are co-linear
		// check if they overlap
		// todo: calculate intersection point
		return std::numeric_limits<float>::infinity();
	}
	if (denominator == 0.0f) {
		// lines are parallel
		return std::numeric_limits<float>::infinity();
	}

	float u = numerator / denominator;
	float t = (coord::cross((originB - originA), s)) / denominator;
	if ((t >= 0.0f) && (t <= 1.0f) && (u >= 0.0f) && (u <= 1.0f)) {
		return t;
	}
	return std::numeric_limits<float>::infinity();
}


Collision AABB::getRayIntersectionFraction(sf::Vector2f origin, sf::Vector2f direction) {
	sf::Vector2f end = origin + direction;
	sf::Vector2f min = this->min();
	sf::Vector2f max = this->max();
	// left
	sf::Vector2f surface = sf::Vector2f(min.x, max.y) - min;
	float minT = getRayIntersectionFractionOfFirstRay(origin, end, min, sf::Vector2f(min.x, max.y));
	float x;
	// bottom
	x = getRayIntersectionFractionOfFirstRay(origin, end, sf::Vector2f(min.x, max.y), max);
	if (x < minT) {
		surface = max - sf::Vector2f(min.x, max.y);
		minT = x;
	}
	else if (x == minT && x != std::numeric_limits<float>::infinity()) {
		return Collision(sf::Vector2f(), std::numeric_limits<float>::infinity());
	}
	// right
	x = getRayIntersectionFractionOfFirstRay(origin, end, max, sf::Vector2f(max.x, min.y));
	if (x < minT) {
		surface = sf::Vector2f(max.x, min.y) - max;
		minT = x;
	}
	else if (x == minT && x != std::numeric_limits<float>::infinity()) {
		return Collision(sf::Vector2f(), std::numeric_limits<float>::infinity());
	}
	// top
	x = getRayIntersectionFractionOfFirstRay(origin, end, sf::Vector2f(max.x, min.y), min);
	if (x < minT) {
		surface = min - sf::Vector2f(max.x, min.y);
		minT = x;
	}
	else if (x == minT && x != std::numeric_limits<float>::infinity()) {
		return Collision(sf::Vector2f(), std::numeric_limits<float>::infinity());
	}

	// ok, now we should have found the fractional component along the ray where we collided
	return Collision(coord::leftNormal(surface), minT);
}
